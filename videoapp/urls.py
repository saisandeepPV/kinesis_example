from django.urls import path
from . import views

app_name = 'videoapp'
urlpatterns = [
	path('hls/',views.home_hls,name="video_home"),
	path('dash/',views.home_dash,name="video_dash_home")
	]